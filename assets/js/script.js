$('document').ready(function(){

   if($('body').hasClass('home')){
      $('.image-thumbs li').each(function(){
         var thumbnail = $(this);
         var thumbnailSrc = thumbnail.find('img').attr('src');
   
         thumbnail.on('click', function(){
            $(".image-detailed img").attr('src', thumbnailSrc);
         });
      })
   
   
      $(window).scroll(function(){
         var scrollTcta = $('.Siclo-c-cta').offset().top;
   
         if ($(this).scrollTop() + $(this).height() > scrollTcta) {
            $('.Siclo-c-productDetails').fadeOut();
         } else {
            $('.Siclo-c-productDetails').fadeIn();
         }
      })
   }

   if($('body').hasClass('instructors-template')){
      function getTrainersList(callback) {
         var xhr = new XMLHttpRequest();
         xhr.addEventListener("readystatechange", () => {
            if (xhr.readyState === 4) {
               if (xhr.status === 200) {
                     // request succesful
                     var response = xhr.responseText;
                     var json = JSON.parse(response);
                     var result = json;
                     callback(result);
               } else {
                     var response = xhr.responseText;
                     var json = JSON.parse(response);
                     // error
                     console.log(json.error);
               }
            }
         });
         xhr.open("GET", "https://api.siclo.com/api/v2/plus/instructors/?format=json&page_size=-1", true);
         xhr.send();
      }

      function organizeInstructors(array, callback){
         var dividedList = [];
         var total = array.length;

         while(array.length) {
            dividedList.push(array.splice(0,10));
         }
         callback(dividedList, total);
      }

      function renderInstructors(instructorsList, total, callback){
         var itemsProcessed = 0;

         instructorsList.forEach(function(team, index){
            var listIndex = index
            $('.Siclo-l-instructores').append('<div class="Siclo-c-instructores" data-index="' + listIndex +'"></div>');
            $('.Siclo-l-instructores__paginacion').append('<span data-page="' + listIndex +'"></span>')
            team.forEach(function(member){
               var quote = member.interests ? '<div class="this-quote">' + member.interests.inspiration  + '</div>' : '';
               var name = member.nombre ? '<h3 class="this-name">' + member.nombre  + '</h3>' : '';
               var picture = member.face_photo !== "pendiente" ? '<div class="this-image"><img src="' + member.face_photo  + '" alt="'+ member.nombre +'" title="'+ member.nombre +'" /></div>' : '';

               itemsProcessed++;

               $('[data-index='+ listIndex +']').append(
                  '<div class="Siclo-i-instructor" id="' + member.id + '">' +
                     quote +
                     picture +
                     name +
                  '</div>'
               )

               if(itemsProcessed === total){
                  callback();
               };
            })
         });
      }

      getTrainersList(function(result){
         organizeInstructors(result, function(instructorsList, totalArray){
            renderInstructors(instructorsList, totalArray, function(){
               $('.Siclo-c-instructores[data-index=0]').addClass('active');

               $('.Siclo-l-instructores__paginacion span').each(function(){
                  var index = $(this).attr('data-page');

                  $(this).on('click', function(){
                     $('.Siclo-c-instructores').removeClass('active');
                     $('[data-index=' + index +']').addClass('active');
                  })
               })
            });
         });
      });
   }
})